//
//  DamageCalculator.swift
//  SoloSpace
//
//  Created by Anders Vinblad on 2019-07-19.
//  Copyright © 2019 Anders Vinblad. All rights reserved.
//

import Foundation

class DamageCalculator {
    var gameData = GameData.shared
    var damage = 0
    
    var critChance = 0.0
    var critMulti = 1.0
    var crit = false
    var armorPen = 0.0
    
    var weaponLeft = ""
    var weaponRight = ""
    
    init() {
        damage = Int(gameData.damage)
        critMulti = gameData.critMulti
        critChance = gameData.critChance
       // armorPen = gameData.armorPen
        weaponLeft = gameData.shipWeaponLeftName
        weaponRight = gameData.shipWeaponRightName
        
        if (weaponLeft == "weaponLeft") {
            damage = Int(Double(damage) * 1.25)
        } else {
            armorPen += 0.4
        }
        
        if (weaponRight == "weaponRight") {
            damage = Int(Double(damage) * 1.25)
        } else {
            armorPen += 0.4
        }
    }
    
    public func damage (alien:Aliens) -> (Int, Bool){
        var totalDamage = Double(damage)
        crit = false
        //crit?
        let randomD = Double.random(in: 0.0 ... 100.0)
        if (critChance >= randomD) {
            totalDamage = Double(damage) * critMulti
            crit = true
        }
        
        totalDamage -= Double(alien.armor!) * (1.0 - armorPen)
        
        if (totalDamage <= 1.0) {
            totalDamage = 1.0
        }
        
        return (Int(totalDamage), crit)
    }
}
