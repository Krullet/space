//
//  GameData.swift
//  SoloSpace
//
//  Created by Anders Vinblad on 2017-05-22.
//  Copyright © 2017 Anders Vinblad. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class GameData {
    static let shared = GameData()
	let defaults = UserDefaults.standard
    //Default values
    var score = Int64(0)
    var difficulty = 1.0
    var attackRate = 0.3
    var attackRateLevel = 1.0
    var damage = 1.0
    var critMulti = 2.0
    var critChance = 15.0
    var armorPen = 0
    var bombs = 3
    var currentWeapon = "wep2"
    var shipName = "playerRed"
    var shipFrontName = "front"
    var shipWeaponLeftName = "weaponLeft"
    var shipWeaponRightName = "weaponRight"
    var shipBackName = "back"

	func loadData(){
		
		let arr = defaults.object(forKey:"SavedArray") as? [String] ?? [String]()
		if ((arr.count == 11)){
			score = Int64(arr[0])!
			difficulty = Double(arr[1])!
			attackRate = Double(arr[2])!
			currentWeapon = arr[3]
			shipName = arr[4]
            damage = Double(arr[5])!
            bombs = Int(arr[6])!
            critMulti = Double(arr[7])!
            critChance = Double(arr[8])!
            armorPen = Int(arr[9])!
            attackRateLevel = Double(arr[10])!
        } else {
            score = Int64(0)
            difficulty = 1.0
            attackRate = 0.3
            damage = 1
            bombs = 3
            currentWeapon = "wep2"
            shipName = "playerRed"
            critMulti = 2.0
            critChance = 5.0
            armorPen = 0
            attackRateLevel = 1
            saveData()
        }
	}
	func saveData(){
		var arr = [String]()
		arr.append(String(score))
		arr.append(String(difficulty))
        arr.append(String(attackRate))
		arr.append(currentWeapon)
		arr.append(shipName)
        arr.append(String(damage))
        arr.append(String(bombs))
        arr.append(String(critMulti))
        arr.append(String(critChance))
        arr.append(String(armorPen))
        arr.append(String(attackRateLevel))

		defaults.set(arr, forKey: "SavedArray")
	}
    
    func upgrade(option:String) -> Int64 {
        var cost:Int64
        cost = 0
        
        switch option {
        case "weapon":
            if (score >= Int64(pow(damage, 2) * 200)) {
                score -= Int64(pow(damage, 2) * 200)
                damage = damage + 1
                cost = Int64(pow(damage, 2) * 200)
                saveData()
            }
            cost = Int64(pow(damage, 2) * 200)
        case "speed":
            if (score >= Int64(pow(attackRateLevel, 2) * 200)) {
                score -= Int64(pow(attackRateLevel, 2) * 200)
                attackRate = attackRate * 0.85
                attackRateLevel = attackRateLevel + 1
                cost = Int64(pow(attackRateLevel, 2) * 200)
                saveData()
            }
            cost = Int64(pow(attackRateLevel, 2) * 200)
        case "crit":
            if (score >= Int64(pow(critMulti, 12) / 20)) {
                score -= Int64(pow(critMulti, 12) / 20)
                critMulti += 0.1
                critChance += 0.05 * (100 - critChance)
                cost = Int64(pow(critMulti, 12) / 20)
                saveData()
            }
            cost = Int64(pow(critMulti, 12) / 20)
        default:
            return cost
        }
        return cost
    }
    
    func getUpgradeCost (option:String) -> Int64{
        var cost:Int64
        cost = 0
        
        switch option {
        case "weapon":
            cost = Int64(pow(damage, 2) * 200)
        case "speed":
            cost = Int64(pow(attackRateLevel, 2) * 200)
        case "crit":
            cost = Int64(pow(critMulti, 12) / 20)
        default:
            return cost
        }
        return cost
    }
    
    func reset () {
        score = 0
        difficulty = 1.0
        attackRate = 1.0
        damage = 1.0
        critMulti = 2.0
        critChance = 15.0
        armorPen = 0
        attackRateLevel = 1.0
        
        
        currentWeapon = "wep2"
        shipName = "playerRed"
        shipFrontName = "front"
        shipWeaponLeftName = "weaponLeft"
        shipWeaponRightName = "weaponRight"
        shipBackName = "back"
        
        saveData()
    }
}
