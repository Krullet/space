//
//  Aliens.swift
//  SoloSpace
//
//  Created by Anders Vinblad on 2017-05-11.
//  Copyright © 2017 Anders Vinblad. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class Aliens: SKSpriteNode {
    var stringName:String?
    var hp:Int?
    var armor:Int?
    var imageName:String?
    var actionArray = [SKAction]()
    
	init(normalAlien:String, difficulty:Double){
        //textures
        stringName = "normalAlien"
        var possibleAliens = ["enemyGreen", "enemyYellow", "enemyPinkBoss", "starAlien"]
        possibleAliens = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: possibleAliens) as! [String]
        let texture = SKTexture(imageNamed: possibleAliens[0])
        super.init(texture:texture, color: SKColor.clear, size: CGSize(width: 150, height: 150))
        
        //MISC
		self.hp = Int(difficulty)
        
        self.armor = (Int(pow(difficulty, 1.3) / 2) - 1)
        
    }
    
	init(bigAlien:String, difficulty:Double){
        stringName = bigAlien
        let texture = SKTexture(imageNamed: bigAlien)
        super.init(texture:texture, color: SKColor.clear, size: CGSize(width: 500, height: 500))
        self.hp = Int(((difficulty * difficulty) / 2) * Double(6)) + 5
        self.armor = (Int(difficulty / 2))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
