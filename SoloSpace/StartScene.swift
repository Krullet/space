//
//  StartScene.swift
//  SoloSpaceV2
//
//  Created by Anders Vinblad on 2017-05-09.
//  Copyright © 2017 Anders Vinblad. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreData

class StartScene: SKScene {
	
	var hasPickedLevel = false
    var shipOrWeaponSelect = true
	var shipNames = [String]()
    var weaponNames = [String]()
    
    var frontNames = [String]()
    var backNames = [String]()
    var loadOutOption = 0
    var currentLoadOutSelection = 0
    
    var loadoutOptions = [String]()
    
	var currentLevelRow = 1
    var currentLevelNr = 1
	var currentShipNr = 0
	var NrOfShips = 4
    var currentShip = 0
    var currentWeapon = 0
    var numberOfWeapons = 3
    var started = false
    let gameData = GameData.shared
    var level = [SKSpriteNode]()
    var backgroundMusic: SKAudioNode!
    var entities = [GKEntity]()
    var graphs = [String : GKGraph]()
	var upgradeButton :		SKSpriteNode!
    var newGameButton :     SKSpriteNode!
    var levelSelectButton:  SKSpriteNode!
    var loadoutButton :     SKSpriteNode!
    var highScoreLabel:     SKLabelNode!
    private var lastUpdateTime : TimeInterval = 0
    private var label : SKLabelNode?
    var starField:SKEmitterNode!
    var starField2:SKEmitterNode!
    let displaySize: CGRect = UIScreen.main.bounds
    var menuItems = [SKSpriteNode]()
    var loadoutMenuItems = [SKSpriteNode]()
	var loadoutMenuLabels = [SKLabelNode]()
	var levelMenuItems = [SKSpriteNode]()
	var shipMenuItems = [SKSpriteNode]()
	var weaponMenuItems = [SKSpriteNode]()
	var boostMenuItems = [SKSpriteNode]()

    override func sceneDidLoad() {
        if (started == false){
			
        startMenu()
        starField = SKEmitterNode(fileNamed: "Starfield")
        starField.position = CGPoint(x: 0, y: 1400)
        starField.advanceSimulationTime(50)
        starField.zPosition = -1
        self.addChild(starField)
        
        starField2 = SKEmitterNode(fileNamed: "Starfield2")
        starField2.position = CGPoint(x: 0, y: 1400)
        starField2.advanceSimulationTime(50)
        starField2.zPosition = -1
        self.addChild(starField2)
        
        // Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
			
		}
    }
    
    func startMenu(){
        if (highScoreLabel != nil){
            highScoreLabel.removeFromParent()
        }
        self.run(SKAction.wait(forDuration: 0.15)){
            //gameData.saveData()
            self.hasPickedLevel = false
            self.gameData.loadData()
            if self.gameData.difficulty >= 5{
                self.currentLevelRow = Int(self.gameData.difficulty/5.0)
            }
            // self.menuItems.removeAll()
            
            self.removeChildren(in: self.level)
            self.removeChildren(in: self.levelMenuItems)
            self.removeChildren(in: self.loadoutMenuItems)
            self.removeChildren(in: self.shipMenuItems)
            self.level.removeAll()
            self.levelMenuItems.removeAll()
            self.loadoutMenuItems.removeAll()
            self.shipMenuItems.removeAll()
            
            let pulseUp = SKAction.scale(to: 1.1, duration: 1.5)
            let pulseDown = SKAction.scale(to: 1, duration: 1.5)
            let pulse = SKAction.sequence([pulseUp, pulseDown])
            let repeatPulse = SKAction.repeatForever(pulse)
            
            self.newGameButton = SKSpriteNode(imageNamed: "quickStart")
            self.newGameButton.position = CGPoint(x: 0, y: 250)
            self.newGameButton.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
            self.newGameButton.zPosition = 1
            self.newGameButton.run(repeatPulse)
            
            
            self.levelSelectButton = SKSpriteNode(imageNamed: "selectLevel")
            self.levelSelectButton.position = CGPoint(x: 0, y: 0)
            self.levelSelectButton.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
            self.levelSelectButton.zPosition = 1
            self.levelSelectButton.run(repeatPulse)
            
            self.loadoutButton = SKSpriteNode(imageNamed: "loadout")
            self.loadoutButton.position = CGPoint(x: 0, y: -250)
            self.loadoutButton.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
            self.loadoutButton.zPosition = 1
            self.loadoutButton.run(repeatPulse)
            
            self.upgradeButton = SKSpriteNode(imageNamed: "UpgradeButton")
            self.upgradeButton.position = CGPoint(x: 0, y: -500)
            self.upgradeButton.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
            self.upgradeButton.zPosition = 1
            self.upgradeButton.run(repeatPulse)
            
            if (self.highScoreLabel == nil){
                self.highScoreLabel = SKLabelNode(text: "Upgrade-Points: " + String(self.gameData.score))
                self.highScoreLabel.position = CGPoint(x:0, y: 450)
                self.highScoreLabel.fontName = "AmericanTypewriter-Bold"
                self.highScoreLabel.fontSize = 40
                self.highScoreLabel.fontColor = UIColor.white
                self.highScoreLabel.run(repeatPulse)
            }
            
            if(!self.menuItems.isEmpty) {
                self.removeChildren(in: self.menuItems)
                self.menuItems.removeAll()
            }
            self.menuItems.append(self.newGameButton)
            self.menuItems.append(self.levelSelectButton)
            self.menuItems.append(self.loadoutButton)
            self.menuItems.append(self.upgradeButton)
            
            
            print("STARTMENU")
            self.removeChildren(in: self.menuItems)
            self.highScoreLabel.removeFromParent()
            if self.menuItems.count >= 1{
                for levels in 0...self.menuItems.count-1{
                    self.addChild(self.menuItems[levels])
                }
                self.addChild(self.highScoreLabel)
            }
        }
    }
    
    
    func levelSelect(){
        self.run(SKAction.wait(forDuration: 0.15)){
            self.removeChildren(in: self.menuItems)
            self.menuItems.removeAll()
            
            
            self.highScoreLabel.removeFromParent()
            self.removeChildren(in: self.levelMenuItems)
            self.levelMenuItems.removeAll()
            let pulseUp = SKAction.scale(to: 1.08, duration: 1)
            let pulseDown = SKAction.scale(to: 1, duration: 1)
            let pulse = SKAction.sequence([pulseUp, pulseDown])
            let repeatPulse = SKAction.repeatForever(pulse)
            
            for levels in 0...4{
            
			var tempLevel = SKSpriteNode(imageNamed: "EmptyButton")
            tempLevel.run(repeatPulse)
			tempLevel.position = CGPoint(x: 0, y: (500 - 200 * levels))
			tempLevel.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
			tempLevel.zPosition = 1
			let tempLevelLabel = SKLabelNode(text: "level " + String((levels + 1) + ((self.currentLevelRow-1)*5)))
			tempLevelLabel.fontName = "AmericanTypewriter"
			tempLevelLabel.fontColor = UIColor.black
			tempLevelLabel.fontSize = 45
			tempLevelLabel.position.y -= 20
			tempLevelLabel.zPosition = 2
			tempLevel.addChild(tempLevelLabel)
            self.levelMenuItems.append(tempLevel)
			self.level.append(tempLevel)
			self.addChild(tempLevel)
                
            }
		var backButton = SKSpriteNode(imageNamed: "ArrowLeft")
		backButton.position = CGPoint(x: -self.size.width/2 + 70, y: -self.size.height/2.8)
		self.levelMenuItems.append(backButton)
		self.addChild(backButton)
		
		var nextButton = SKSpriteNode(imageNamed: "ArrowRight")
		nextButton.position = CGPoint(x: self.size.width/2 - 70, y: -self.size.height/2.8)
		self.levelMenuItems.append(nextButton)
		self.addChild(nextButton)
		
		var backToMenuButton = SKSpriteNode(imageNamed: "backButton")
		backToMenuButton.position = CGPoint(x: 0, y: -self.size.height/2.8)
		self.addChild(backToMenuButton)
		self.levelMenuItems.append(backToMenuButton)
		}
    }
	
	func upgradeMenu(){
		self.run(SKAction.wait(forDuration: 0.15)){

            self.removeChildren(in: self.menuItems)
            self.menuItems.removeAll()
            self.removeChildren(in: self.levelMenuItems)
            self.levelMenuItems.removeAll()
            self.loadoutMenuItems.removeAll()
            
		if self.loadoutMenuItems.isEmpty{
			self.loadoutMenuLabels.removeAll()
			let pulseUp = SKAction.scale(to: 1.1, duration: 1.5)
			let pulseDown = SKAction.scale(to: 1, duration: 1.5)
			let pulse = SKAction.sequence([pulseUp, pulseDown])
			let repeatPulse = SKAction.repeatForever(pulse)
				for levels in 0...4{
					var tempLevel = SKSpriteNode(imageNamed: "EmptyButton")
					tempLevel.run(repeatPulse)
					tempLevel.position = CGPoint(x: 0, y: (300 - 200 * levels))
					tempLevel.size = CGSize(width: self.displaySize.width / 1.1, height: self.displaySize.height / 7)
					tempLevel.zPosition = 1
					var tempLevelLabel = SKLabelNode(text: "level ")
					tempLevelLabel.fontName = "AmericanTypewriter"
					tempLevelLabel.fontColor = UIColor.black
					tempLevelLabel.fontSize = 30
					tempLevelLabel.position.y -= 20
					tempLevelLabel.zPosition = 2
					tempLevel.addChild(tempLevelLabel)
					self.loadoutMenuItems.append(tempLevel)
					self.loadoutMenuLabels.append(tempLevelLabel)
				}
            
			self.loadoutMenuLabels[0].text = "Reset"
			self.loadoutMenuLabels[1].text = "Damage " + String(Int(self.gameData.getUpgradeCost(option: "weapon")))
			self.loadoutMenuLabels[2].text = "Speed " + String(Int(self.gameData.getUpgradeCost(option: "speed")))
            self.loadoutMenuLabels[3].text = "Crit " + String(Int(self.gameData.getUpgradeCost(option: "crit")))
            self.loadoutMenuLabels[4].text = "Menu"

		}
            for index in 0...self.loadoutMenuItems.count - 1{
			self.addChild(self.loadoutMenuItems[index])
            }
        }
    }
	
	func resetMenu(){}
	func weaponMenu(){}
	func shipSelect(){
        
		self.run(SKAction.wait(forDuration: 0.15)){
            self.removeChildren(in: self.menuItems)
            self.menuItems.removeAll()
            let pulseUp = SKAction.scale(to: 1.1, duration: 1.5)
			let pulseDown = SKAction.scale(to: 1, duration: 1.5)
			let pulse = SKAction.sequence([pulseUp, pulseDown])
			let repeatPulse = SKAction.repeatForever(pulse)

            if (self.shipNames.isEmpty) {
                self.shipNames.append("playerYellow")
                self.shipNames.append("playerBlue")
                self.shipNames.append("playerRed")
                self.shipNames.append("playerGreen")
                self.shipNames.append("playerWhite")
                self.shipNames.append("back")
                self.shipNames.append("front")

                self.weaponNames.append("weapon1")
                self.weaponNames.append("weapon2")
                self.weaponNames.append("weapon3")
                self.weaponNames.append("weaponLeft")
                self.weaponNames.append("weaponRight")
                
                self.loadoutOptions.append("weaponRight")
            }
			
			var shipSprite = SKSpriteNode(imageNamed: self.shipNames[0])
			shipSprite.run(repeatPulse)
			shipSprite.position = CGPoint(x: 0, y: 0)
			shipSprite.size = CGSize(width: self.displaySize.width, height: self.displaySize.height / 2)
			shipSprite.zPosition = 1
			self.shipMenuItems.append(shipSprite)
			
            var backButton = SKSpriteNode(imageNamed: "ArrowLeft")
            backButton.position = CGPoint(x: -self.size.width/2 + 70, y: 0)
            backButton.setScale(1.2)
            backButton.run(repeatPulse)
            self.shipMenuItems.append(backButton)
            
            var nextButton = SKSpriteNode(imageNamed: "ArrowRight")
            nextButton.position = CGPoint(x: self.size.width/2 - 70, y: 0)
            nextButton.setScale(1.2)
            self.shipMenuItems.append(nextButton)
            
            var backToMenuButton = SKSpriteNode(imageNamed: "StartMenuButton")
            backToMenuButton.position = CGPoint(x: 0, y: -self.size.height/2.8)
            self.shipMenuItems.append(backToMenuButton)
            
            var previousTypeButton = SKSpriteNode(imageNamed: "ArrowLeft")
            previousTypeButton.position = CGPoint(x: -self.size.width/2 + 70, y: -self.size.height/2.8)
            self.shipMenuItems.append(previousTypeButton)
            
            var nextTypeButton = SKSpriteNode(imageNamed: "ArrowRight")
            nextTypeButton.position = CGPoint(x: self.size.width/2 - 70, y: -self.size.height/2.8)
            nextTypeButton.run(repeatPulse)
            self.shipMenuItems.append(nextTypeButton)
			
			for index in 0...self.shipMenuItems.count - 1{
				self.addChild(self.shipMenuItems[index])
			}
		}
	}
	func bombMenu(){}

    func startGame(){
        self.removeAllChildren()
        if let scene = GKScene(fileNamed: "GameScene") {
            
            if let sceneNode = scene.rootNode as! GameScene? {
                
                // Copy gameplay related content over to the scene
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFill
                // Present the scene
                if let view = self.view {
                    
                    view.presentScene(sceneNode, transition: SKTransition.flipHorizontal(withDuration: 0.5))
                    view.ignoresSiblingOrder = true
                    
                }
            }
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
		
    }
    func touchDown(atPoint pos : CGPoint) {
    }
    func touchUp(atPoint pos : CGPoint) {
		
		if  menuItems.isEmpty == false {
			
			if (menuItems[0].contains(pos)){
			//	self.run(SKAction.playSoundFileNamed("select.wav", waitForCompletion: true))
				self.removeChildren(in: menuItems)
				self.highScoreLabel.removeFromParent()
				startGame()
			}
            //LEVEL SELECT
			if (menuItems[1].contains(pos)){
			//	self.run(SKAction.playSoundFileNamed("select.wav", waitForCompletion: true))
				self.removeChildren(in: menuItems)
				self.highScoreLabel.removeFromParent()
				levelSelect()

			}
            //SHIP SELECT
			if (menuItems[2].contains(pos)){
				
				self.removeChildren(in: menuItems)
			//	self.highScoreLabel.removeFromParent()
				shipSelect()
				
			}
			//UPGRADE
			if (menuItems[3].contains(pos)){
                
                self.removeChildren(in: menuItems)
          //      self.run(SKAction.playSoundFileNamed("select.wav", waitForCompletion: true))
                upgradeMenu()
                
			}
		}
		if level.count >= 1 {
			for levels in 0...level.count-1{
				if (level[levels].contains(pos)){
					if(hasPickedLevel == false){
						hasPickedLevel = true
						self.gameData.difficulty = Double((levels + 1) + ((5 * currentLevelRow)) - 5)
						print("current Row: " + String(currentLevelRow))
						print("current Difficulty: " + String(self.gameData.difficulty))
						
						gameData.saveData()
						print(gameData.difficulty)
						print(currentLevelRow)
						startGame()
					}
				}
			}
            
			for levels in 5...levelMenuItems.count-1{
				if levelMenuItems.isEmpty == false{
					if levelMenuItems[levels].contains(pos){
				//		self.run(SKAction.playSoundFileNamed("select.wav", waitForCompletion: true))
						switch levels {
						case 5:
							print("Left Arrow")
							if (currentLevelRow >= 2){
								currentLevelRow -= 1
								print("current Row: " + String(currentLevelRow))
								levelSelect()
							}
						case 6:
							print("Right Arrow")
							currentLevelRow += 1
							levelSelect()
							print("current Row: " + String(currentLevelRow))
						case 7:
							print("Middle")
							startMenu()
							self.removeChildren(in: levelMenuItems)
						default:
							print("default levelMenuItems")
							startMenu()
							self.removeChildren(in: levelMenuItems)
						}
					}
				}
			}
		}
        
		if loadoutMenuItems.count >= 3 {
			for levels in 0...loadoutMenuItems.count-1{
				if loadoutMenuItems.isEmpty == false{
					if loadoutMenuItems[levels].contains(pos){
                        var newCost: Int64
                        newCost = 0
						switch levels {
						case 0:
							////RESET
							self.gameData.reset()
                            self.loadoutMenuLabels[1].text = "Damage " + String(Int(self.gameData.getUpgradeCost(option: "weapon")))
                            self.loadoutMenuLabels[2].text = "Speed " + String(Int(self.gameData.getUpgradeCost(option: "speed")))
                            self.loadoutMenuLabels[3].text = "Crit " + String(Int(self.gameData.getUpgradeCost(option: "crit")))
                            self.highScoreLabel.text = "Upgrade-Points: " + String(gameData.score)
                            print("RESET")
							
						case 1:
                            print("WEAPON")
                            newCost = self.gameData.upgrade(option: "weapon")
                            self.loadoutMenuLabels[1].text = "Damage " + String(newCost)
                            self.highScoreLabel.text = "Upgrade-Points: " + String(gameData.score)
                            
						case 2:
                            print("SPEED")
                            if (gameData.attackRate >= 0.05) {
                                newCost = self.gameData.upgrade(option: "speed")
                                self.loadoutMenuLabels[2].text = "Speed " + String(newCost)
                                self.highScoreLabel.text = "Upgrade-Points: " + String(gameData.score)
                            }
						case 3:
                            print("CRIT")
                            newCost = self.gameData.upgrade(option: "crit")
                            self.loadoutMenuLabels[3].text = "Crit "  + String(newCost)
                            self.highScoreLabel.text = "Upgrade-Points: " + String(gameData.score)
                        case 4:
                            self.removeChildren(in: loadoutMenuItems)
                            loadoutMenuItems.removeAll()
                            self.run(SKAction.wait(forDuration: 0.2)){
                                self.startMenu()
                            }
						default:
							print("default loadoutMenuItems")
						}
					}
				}
			}
		}
        
		if shipMenuItems.count >= 1 {
            numberOfWeapons = weaponNames.count
            NrOfShips = shipNames.count
            
			for levels in 0...shipMenuItems.count-1{
				if shipMenuItems.isEmpty == false{
					if shipMenuItems[levels].contains(pos){
						switch levels {
                            
							//SELECT SHIP OR WEAPON
						case 0:
                            switch loadOutOption {
                            case 0:
                                gameData.shipFrontName = loadoutOptions[currentLoadOutSelection]
                            case 1:
                                gameData.shipBackName = loadoutOptions[currentLoadOutSelection]
                            case 2:
                                gameData.shipWeaponLeftName = loadoutOptions[currentLoadOutSelection]
                            case 3:
                                gameData.shipWeaponRightName = loadoutOptions[currentLoadOutSelection]
                            default:
                                gameData.shipName = loadoutOptions[currentLoadOutSelection]
                            }
                            gameData.saveData()
                            self.highScoreLabel.text = "Selected"
                            
						case 1:
                            //PREVIOUS OPTION
                            self.currentLoadOutSelection = (self.currentLoadOutSelection - 1) % loadoutOptions.count
                            shipMenuItems[0].texture = SKTexture(imageNamed: loadoutOptions[self.currentLoadOutSelection])
                    
						case 2:
                            //NEXT OPTION
                            self.currentLoadOutSelection = (self.currentLoadOutSelection + 1) % loadoutOptions.count
                            shipMenuItems[0].texture = SKTexture(imageNamed: loadoutOptions[self.currentLoadOutSelection])

						case 3:
							startMenu()
							self.removeChildren(in: shipMenuItems)
                        case 4:
                            print("previous list type in list")
                            self.loadoutOptions.removeAll()
                            self.currentLoadOutSelection = 0
                
                            loadOutOption = (loadOutOption - 1) % 4
                            
                            if (loadOutOption == 0) {
                                //front
                                self.highScoreLabel.text = "front"
                                self.loadoutOptions.append("front")
                                self.loadoutOptions.append("front1")
                                self.loadoutOptions.append("playerYellow")

                            } else if (loadOutOption == 1) {
                                //back
                                self.highScoreLabel.text = "back"
                                self.loadoutOptions.append("back")
                                self.loadoutOptions.append("back1")
                                self.loadoutOptions.append("playerYellow")
                            }   else if (loadOutOption == 2) {
                                //weapon left
                                self.highScoreLabel.text = "weapon left"
                                self.loadoutOptions.append("weaponLeft")
                                self.loadoutOptions.append("weaponLeft1")
                                self.loadoutOptions.append("playerYellow")
                                
                            }   else if (loadOutOption == 3) {
                                //weapon right
                                self.highScoreLabel.text = "weapon right"
                                self.loadoutOptions.append("weaponRight")
                                self.loadoutOptions.append("weaponRight1")
                                self.loadoutOptions.append("playerYellow")
                            }
                            
                        case 5:
                            print("next in list")
                            self.loadoutOptions.removeAll()
                            self.currentLoadOutSelection = 0
                            
                            loadOutOption = (loadOutOption + 1) % 4
                            
                            if (loadOutOption == 0) {
                                //front
                                self.highScoreLabel.text = "front"
                                self.loadoutOptions.append("front")
                                self.loadoutOptions.append("front1")
                                self.loadoutOptions.append("playerYellow")
                                
                            } else if (loadOutOption == 1) {
                                //back
                                self.highScoreLabel.text = "back"
                                self.loadoutOptions.append("back")
                                self.loadoutOptions.append("back1")
                                self.loadoutOptions.append("playerYellow")
                            }   else if (loadOutOption == 2) {
                                //weapon left
                                self.highScoreLabel.text = "weapon left"
                                self.loadoutOptions.append("weaponLeft")
                                self.loadoutOptions.append("weaponLeft1")
                                self.loadoutOptions.append("playerYellow")
                                
                            }   else if (loadOutOption == 3) {
                                //weapon right
                                self.highScoreLabel.text = "weapon right"
                                self.loadoutOptions.append("weaponRight")
                                self.loadoutOptions.append("weaponRight1")
                                self.loadoutOptions.append("playerYellow")
                            }
                            
						default:
							print("default shiMenuItems")
						}
					}
				}
				
				
			}
		}
    }
    
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		for t in touches { self.touchDown(atPoint: t.location(in: self)) }
	}
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
		self.run(SKAction.moveBy(x: 100, y: 0, duration: 1))
	}
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		for t in touches { self.touchUp(atPoint: t.location(in: self)) }
	}
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
		for t in touches { self.touchUp(atPoint: t.location(in: self)) }
	}
	override func update(_ currentTime: TimeInterval) {
		// Called before each frame is rendered
		
		// Initialize _lastUpdateTime if it has not already been
		if (self.lastUpdateTime == 0) {
			self.lastUpdateTime = currentTime
		}
		
		// Calculate time since last update
		let dt = currentTime - self.lastUpdateTime
		
        // Update entities
        for entity in self.entities {
            entity.update(deltaTime: dt)
        }
		
        self.lastUpdateTime = currentTime
    }
}
